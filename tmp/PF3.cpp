
/*
Matteo Guerra 2015 - ISS Potential field Collision avoidance
*/


#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/String.h>
#include <tf/transform_listener.h> 
#include <sensor_msgs/PointCloud.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <visualization_msgs/Marker.h>

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <math.h>


#define _USE_MATH_DEFINES

double xPos = 100.0;
double yPos = 100.0;
double wPos = 0.0;
double obs_x = 100.0;
double obs_y = 100.0;
double obs_r = 0.0;
double obs_lw = 0.0;







//void read_odometry(const geometry_msgs::PoseStamped::ConstPtr& odometry)
void read_odometry(const geometry_msgs::PoseWithCovarianceStampedConstPtr& pose)
{   
   double roll, pitch, yaw;
   xPos=pose->pose.pose.position.x; 
   yPos=pose->pose.pose.position.y;

    tf::Quaternion q;
    tf::quaternionMsgToTF(pose->pose.pose.orientation, q);
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    
    if (yaw<0) yaw = yaw + 2* M_PI;
    
    wPos = yaw;

if (wPos < -M_PI) 
    wPos = wPos+2*M_PI;
else
{if (wPos >M_PI)
    {wPos = wPos - 2*M_PI;}
}

printf("x=%f, y=%f, th=%f \n",xPos,yPos,wPos);
}
////////////////////////////////////////////////////////////////////////////////////////////
// read the laser data and define the obstacle "center"
void read_laser(const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
   int i_min, i_max, premier = 0;
   double xmin = -100.0, xmax = 100.0, ymin = -100.0, ymax = 100.0, px, py;
   double dist, ang, far;

   far=0.8;
   i_min = (-M_PI/2 - scan_in->angle_min )/(scan_in->angle_increment);
   i_max = (M_PI/2 - scan_in->angle_min )/(scan_in->angle_increment);
   
   for (int i=i_min;i<=i_max;i++)
   {
       //if ((scan_in->ranges[i] <= 0.60) && (scan_in->ranges[i] >= 0.25))
	if (scan_in->ranges[i] <= far)
           {
            dist =(double) scan_in->ranges[i];
            ang =(double) scan_in->angle_min+i*scan_in->angle_increment;
            px = (dist)*cos(ang);
            py = (dist)*sin(ang);
            if (premier == 0) 
              { 
                xmin = px;
                xmax = px;
                ymin = py;
                ymax = py;
                premier = 1;
               }

            else 
		{
		 if (px<xmin) xmin = px;
		 if (px>xmax) xmax = px;
		 if (py<ymin) ymin = py;
		 if (py>ymax) ymax = py;	
		}
               
            }
/*
// formal definition of the obstacle center
      obs_x = xmin + (xmax-xmin)/2;
      obs_y = ymin + (ymax-ymin)/2;
      obs_lw = atan2(obs_y,obs_x);
*/

//easy definition of the obstacle center
	obs_x = xmin;
        obs_y = ymin;
	obs_lw = atan2(obs_y,obs_x);



      //obs_r = sqrt(pow(obs_x-xmin,2)+pow(obs_y-ymin,2));
      obs_r = far;
   }
     
   

}

//////////////////////////////////////////////////////////////////////////////////////////
double* sign(double valore)
{double to_return;
 double * ret;
 ret = (double*)calloc(1,sizeof(int));
/*
	if(valore>0){to_return = 1;}
	if(valore==0){to_return = 0;}
	if(valore<0){to_return = -1;}
*/
to_return = fabs(valore)/(valore+0.001);


	*(ret+0)=to_return;
 return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////

double* PF(double x_p, double y_p, double theta_p, double obs_dx, double obs_dy)
{
//Collision Avoidance gains and parameters
//ca stands for collision avoidance
    double * control;
    double *segno1, *segno2;
    control = (double*)calloc(3,sizeof(double));
    double dist_g = 100.0, value = 0.0, dist = 100.0, dist_safe=0.8, rho=0.0;
    double theta_d,err;

	double F1,F2;
	double Fatt1=0,Fatt2=0;
	double Frep1=0,Frep2=0;
	double V1=0,V2=0;

	double nu = 0.2;
	double Psi = 0.2;

	double alpharep=4;

	double epsilon = 0.11;

	double umax=0.15;
	double wmax=0.4;

//printf("dentro PF x=%f, y=%f, th=%f \n",x_p,y_p,theta_p);
//printf("dentro PF obs_x=%f, obs_y=%f \n",obs_dx,obs_dy);

if (theta_p < -M_PI) 
    theta_p = theta_p+2*M_PI;
else
{if (theta_p >M_PI)
    {theta_p = theta_p - 2*M_PI;}
}


///////// attractive force

dist_g = sqrt(pow(x_p,2)+pow(y_p,2));
printf("dist_g=%f \n",dist_g);

dist = sqrt(pow((x_p-obs_dx),2)+pow((y_p-obs_dy),2));
printf("dist=%f \n",dist);

if (dist_g <= nu) 
{ Fatt1 = -2*x_p;
  Fatt2 = -2*y_p;
printf("vicino Fatt1=%f Fatt2=%f \n",Fatt1,Fatt2);
}

if (dist_g > Psi)
{
    Fatt1 = -x_p/dist_g;
    Fatt2 = -y_p/dist_g;
printf("lontano Fatt1=%f, Fatt2=%f \n",Fatt1,Fatt2);
}

///////// repulsive force
if (dist<dist_safe)
{    
        value =pow(dist_safe,2)-pow(dist,2);
	Frep1 = 4*alpharep*fmax(0,value)*(x_p-obs_dx);
	Frep2 = 4*alpharep*fmax(0,value)*(y_p-obs_dy);  
printf("Frep1=%f, Frep2=%f \n",Frep1,Frep2);                
}
///////// composition
F1 = Fatt1 +Frep1;
F2 = Fatt2 +Frep2;
///////// Eventual V
if (sqrt(pow(F1,2)+pow(F2,2))<=epsilon)
{
	rho = y_p-(obs_dy/obs_dx)*x_p;
        segno1 =sign(rho);

	V1 = (((*segno1)*epsilon)/dist_g)*y_p;
	V2 = (((*segno1)*epsilon)/dist_g)*(-x_p);
}
else 
{
V1=0;
V2=0;
}
/////////
F1 = Fatt1+Frep1-V1;
F2 = Fatt2+Frep2-V2;

theta_d=atan2(F2,F1);
printf("theta_d=%f \n",theta_d);
//theta_d=atan2(y_p,x_p)+M_PI;
//printf("theta_d=%f \n",theta_d);

err = theta_d-theta_p;
printf("err=%f \n",err);

if (err < -M_PI) 
    err = err+2*M_PI;
else
{if (err >M_PI)
    {err = err - 2*M_PI;}
}
printf("dopo err=%f \n",err);
segno2 = sign(err);

 *(control+1) = wmax*sqrt(fabs(err))*(*(segno2));
/*
if (fabs(err)<M_PI/2)
*/
 *(control+0) = (umax/(1+epsilon))*(sqrt(pow(F1,2)+pow(F2,2)));
/*
else
 *(control+0) = 0.0;
*/
*(control+2)=theta_d;

    //check max linear
    if (*(control+0)>umax)  {*(control+0)=umax;}

    //check max angular
    if (*(control+1)>wmax) {*(control+1)=wmax;}
    

    return control;

}


int main (int argc, char **argv)
{

  ros::init(argc,argv,"mgcontrol_potentialfield");
  ros::NodeHandle n;
  ros::Rate loop_rate(10); //10Hz
  ros::Publisher  pub_cmd=n.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/teleop",100);
  //ros::Subscriber sub1 = n.subscribe("slam_out_pose",100, read_odometry);
  ros::Subscriber sub1 = n.subscribe("amcl_pose",100, read_odometry);
  ros::Subscriber sub2 = n.subscribe("scan",100, read_laser);
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);
ros::Publisher marker_pub2 = n.advertise<visualization_msgs::Marker>("visualization_marker2", 10);

  geometry_msgs::Twist to_pub; 
  ros::Time begin = ros::Time::now();
  double begin_sec = begin.toSec();
 double safety;
  visualization_msgs::Marker points;
  visualization_msgs::Marker points2;
  
  
  double * CAv=NULL;
  int FLAG = 0, COUNT = 1, SAFE =1;
  int stat = 0;
 double obsG_x, obsG_y, min;

printf("xPos = %g\t yPos = %g\n",xPos,yPos);
printf("FLAG = %d\n",FLAG);
		      
while (ros::ok())
  {
    
    if (sqrt(pow(xPos,2)+pow(yPos,2))>0.1)
		    {
			min = sqrt(pow(obs_x,2)+pow(obs_y,2));
  			//old laser position
      			//obsG_x = (xPos+0.14)+ min*cos(wPos+obs_lw);
			//new laser pos
			obsG_x = (xPos)+ min*cos(wPos+obs_lw);
      			obsG_y = (yPos)+ min*sin(wPos+obs_lw);

			    CAv = PF(xPos,yPos,wPos,obsG_x,obsG_y);
			    to_pub.linear.x = *(CAv+0);
				printf("CAv vel %f \n",*(CAv+0));
			    to_pub.angular.z = *(CAv+1);
			    printf("CAv ang %f \n",*(CAv+1));

			if ((min!=0) && (min < 0.20))
			{
			printf("SAFETY STOP %g\n",min);
			printf("obs_x %g, obs_y %g\n",obs_x,obs_y);
			printf("obsG_x %g, obsG_y %g\n",obsG_x,obsG_y);
			to_pub.linear.x = 0;
			to_pub.angular.z = 0;
			//pub_cmd.publish(to_pub);
			
			}


/////////////// velocity vector print on GUI.
points.header.frame_id ="/map";
points.header.stamp =ros::Time::now();
points.ns = "points_and_lines";
points.action = visualization_msgs::Marker::ADD;
points.id = 0;
points.type = visualization_msgs::Marker::ARROW;
points.scale.x = 0.7;
points.scale.y = 0.05;
points.color.a = 1.0;
points.color.r = 1.0;
points.color.g = 0.0;
points.color.b = 0.0;
points.pose.position.x = xPos;
points.pose.position.y = yPos;
points.pose.position.z = 0.0;
//
geometry_msgs::Quaternion qarrow=tf::createQuaternionMsgFromYaw(*(CAv+2)); 
//
points.pose.orientation = qarrow;
//geometry_msgs::Point p;
//points.points.push_back(p);
marker_pub.publish(points);
//////////////

/////////////// obstacle "center" point print on GUI.
points2.header.frame_id ="/map";
points2.header.stamp =ros::Time::now();
points2.ns = "points_and_lines2";
points2.action = visualization_msgs::Marker::ADD;
points2.id = 1;
points2.type = visualization_msgs::Marker::POINTS;
points2.scale.x = 0.11;
points2.scale.y = 0.11;
points2.scale.z = 0;
points2.color.a = 1.0;
points2.color.r = 0.0;
points2.color.g = 0.0;
points2.color.b = 1.0;

geometry_msgs::Point p2;

p2.x=obsG_x;
p2.y=obsG_y;
if (!points2.points.empty())
points2.points.pop_back();
points2.points.push_back(p2);
/*
points2.pose.position.x = p2.x;
points2.pose.position.y = p2.y;
points2.pose.position.z = 0.0;
points2.pose.orientation.w = 1.0;
*/
marker_pub2.publish(points2);
//////////////




			    pub_cmd.publish(to_pub);
			    ros::spinOnce();
			    loop_rate.sleep();
		   }

   if (sqrt(pow(xPos,2)+pow(yPos,2))<=0.1)
    {printf("Task Completed\n");
    to_pub.linear.x = 0;
    to_pub.angular.z = 0;
    pub_cmd.publish(to_pub);
    break;
    }

   
    }
  return 0;
}
  
