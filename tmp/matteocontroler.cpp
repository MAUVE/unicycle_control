
/*
Matteo Guerra 2015 - Supervisory Control of Unicycle under input disturbances
*/


#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/String.h>
#include <tf/transform_listener.h> 
#include <sensor_msgs/PointCloud.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
//#include <laser_geometry/laser_geometry.h>
#include <matteocontroler/Bpoint.h>

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <math.h>


#define _USE_MATH_DEFINES

double xPos = 0.0;
double yPos = 0.0;
double wPos = 0.0;
double obs_x = 10.0;
double obs_y = 10.0;
double obs_r = 0.0;
double obs_lw = 0.0;


/*
Function to evaluate the intersection between 2 circunferences - hard coded solution 
*/

double* IntCirc(double a,double b,double c, double d,double r1, double r2)
{   
  double alpha,beta,A,B,C,xsol1,xsol2,ysol1,ysol2;
  double * XY;
  XY = (double*)calloc(4,sizeof(double));
  
  alpha = (pow(a,2)+pow(b,2)-r1+r2-pow(c,2)-pow(d,2))/(2*b-2*d);

  beta = (2*c-2*a)/(2*b-2*d);

  A = (1+pow(beta,2));

  B = -2*a+2*alpha*beta-2*b*beta;

  C = pow(a,2)+pow(alpha,2)-2*b*alpha+pow(b,2)-r1;

  xsol1 = (-B-sqrt(pow(B,2)-4*A*C))/(2*A);
  xsol2 = (-B+sqrt(pow(B,2)-4*A*C))/(2*A);

  ysol1 = alpha +beta*xsol1;
  ysol2 = alpha +beta*xsol2;

  *(XY+0)=xsol1;
  *(XY+1)=ysol1;
  *(XY+2)=xsol2;
  *(XY+3)=ysol2;

  return XY;

 }

/*
Function to evaluate the intersection a circunference and a line - hard coded solution 
*/

double* IntCircLin(double m,double xo,double yo, double a,double b, double rq)
{   
  double c,A,B,C,xsol1,xsol2,ysol1,ysol2;
  double * XY;
  XY = (double*)calloc(4,sizeof(double));
  
  c=-m*xo+yo;

  A = 1+pow(m,2);
  B = -2*a+2*m*c-2*b*m;
  C = pow(a,2)+pow(c,2)-2*b*c+pow(b,2)-rq;

  xsol1 = (-B-sqrt(pow(B,2)-4*A*C))/(2*A);
  xsol2 = (-B+sqrt(pow(B,2)-4*A*C))/(2*A);

  ysol1 = m*xsol1+c;
  ysol2 = m*xsol2+c;

  *(XY+0)=xsol1;
  *(XY+1)=ysol1;
  *(XY+2)=xsol2;
  *(XY+3)=ysol2;

  return XY; 

 }


//void read_odometry(const geometry_msgs::PoseStamped::ConstPtr& odometry)
// to get the robot position in the map
void read_odometry(const geometry_msgs::PoseWithCovarianceStampedConstPtr& pose)
{   
   double roll, pitch, yaw;
   xPos=pose->pose.pose.position.x; 
   yPos=pose->pose.pose.position.y;

    tf::Quaternion q;
    tf::quaternionMsgToTF(pose->pose.pose.orientation, q);
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    
    if (yaw<0) yaw = yaw + 2* M_PI;
    
    wPos = yaw;
}
////////////////////////////////////////////////////////////////////////////////////////////
// to read the laser data
void read_laser(const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
   int i_min, i_max, premier = 0;
   double xmin = -10.0, xmax = 10.0, ymin = -10.0, ymax = 10.0, px, py;
   double dist, ang;

// to fix the field of view of the laser
   i_min = (-M_PI/4 - scan_in->angle_min )/(scan_in->angle_increment);
   i_max = (M_PI/4 - scan_in->angle_min )/(scan_in->angle_increment);
   
   for (int i=i_min;i<=i_max;i++)
   {
       //if ((scan_in->ranges[i] <= 0.60) && (scan_in->ranges[i] >= 0.25))
	if (scan_in->ranges[i] <= 0.60)
           {
            dist =(double) scan_in->ranges[i];
            ang =(double) scan_in->angle_min+i*scan_in->angle_increment;
            px = (dist)*cos(ang);
            py = (dist)*sin(ang);
            if (premier == 0) 
              { 
                xmin = px;
                xmax = px;
                ymin = py;
                ymax = py;
                premier = 1;
               }
            else 
		{
		 if (px<xmin) xmin = px;
		 if (px>xmax) xmax = px;
		 if (py<ymin) ymin = py;
		 if (py>ymax) ymax = py;	
		}
               
            }
      obs_x = xmin + (xmax-xmin)/2;
      obs_y = ymin + (ymax-ymin)/2;
      obs_lw = atan2(obs_y,obs_x);
      obs_r = sqrt(pow(obs_x-xmin,2)+pow(obs_y-ymin,2));
   }
     
   

}

//////////////////////////////////////////////////////////////////////////////////////////
// standard sign function - can be omitted and the standard tanh can be used in case of errors
int* sign(double valore)
{int to_return;
 int * ret;
 ret = (int*)calloc(1,sizeof(int));

	if(valore>0){to_return = 1;}
	if(valore==0){to_return = 0;}
	if(valore<0){to_return = -1;}

	*(ret+0)=to_return;
 return ret;
}

/////////////////////////////////////////////////////////////////////////////////////////
//alternative stabilization point defined in case of obstacle
double* Bpoint(double x_p,double y_p,double theta_p, double x_o,double y_o, double dmax)
{
  double xb,yb, theta_b, psip, theta_lim,psi;
  double r1,r2,rq;
  double theta_i, ro_min, ro, R1, y2i, xm, ym;
  double * B, * xtg, * xtg1, * xtg2, *x2, * x;
  double theta_ptg1, theta_ptg2, m_ptg1, m_ptg2;
  double limax1x, limax1y, limax11x, limax11y, limax12x, limax12y;
  double limax2x, limax2y, limax21x, limax21y, limax22x, limax22y;
  double m, B1x, B1y, B2x, B2y;
  double B3x, B3y, B4x, B4y;
  
  
  B = (double*)calloc(2,sizeof(double));
  
//distance parameters depending on the robot size
  ro_min = obs_r + 0.30;
  ro = ro_min+0.30;
  R1 = 0.15+ro;
  
  theta_i = atan2(y_p-y_o,x_p-x_o);
  y2i = sqrt(pow(x_p-x_o,2)+pow(y_p-y_o,2));
  xm = x_o+0.5*y2i*cos(theta_i);
  ym = y_o+0.5*y2i*sin(theta_i);


  r1 = 0.5*pow(y2i,2);
  r2 = pow(ro_min,2);

  xtg = IntCirc(xm,ym,x_o,y_o,r1,r2);


  theta_ptg1 =  atan2(*(xtg+0)-y_o,*(xtg+1)-x_o);
  m_ptg1 = -1/tan(theta_ptg1);
  
  rq = pow(R1,2);
  xtg1 =IntCircLin( m_ptg1, x_p, y_p,  x_o, y_o,  rq);


  limax11x = *(xtg1+0);
  limax11y = *(xtg1+1);

  limax12x = *(xtg1+2);
  limax12y = *(xtg1+3);


  if ( (sqrt(pow(limax11x,2)+pow(limax11y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) )
    {
    limax1x = limax11x;
    limax1y = limax11y;
    }
  else
    {
    limax1x = limax12x;
    limax1y = limax12y;
    } 

  theta_ptg2 =  atan2(*(xtg+2)-y_o,*(xtg+3)-x_o);
  m_ptg2 = -1/tan(theta_ptg2);

  rq = pow(R1,2);
  xtg2 =IntCircLin( m_ptg2, x_p, y_p,  x_o, y_o,  rq);

  limax21x = *(xtg2+0);
  limax11y = *(xtg2+1);

  limax12x = *(xtg2+2);
  limax12y = *(xtg2+3);

  if ( (sqrt(pow(limax21x,2)+pow(limax21y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) )
    {
    limax2x = limax21x;
    limax2y = limax21y;
    }
  else
    {
    limax2x = limax22x;
    limax2y = limax22y;
    } 

 
  m =-1/tan(theta_i);
 
  rq = pow(R1,2);
  x2 =IntCircLin( m, x_p, y_p,  x_o, y_o,  rq);

  B1x = *(x2+0);
  B1y = *(x2+1);
  B2x = *(x2+2);
  B2y = *(x2+3);

if ( ( (sqrt(pow(B1x,2)+pow(B1y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) ) || ( (sqrt(pow(B2x,2)+pow(B2y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) ) )
{
 if ( (sqrt(pow(B1x,2)+pow(B1y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) )
    {
    xb = B1x;
    yb = B1y;
    }
  else
    {
    xb = B2x;
    yb = B2y;
    } 
}

else
{
 r1 =sqrt(pow(x_p,2)+pow(y_p,2));
 r2 =pow(R1,2);
 x = IntCirc(0,0,x_o,y_o,r1,r2);

 B3x = *(x+0);
 B3y = *(x+1);
 B4x = *(x+2);
 B4y = *(x+3);

 if ( (sqrt(pow(B3x,2)+pow(B3y,2)))  < (sqrt(pow(x_p,2)+pow(y_p,2))) )
    {
    xb = B3x;
    yb = B3y;
    }
  else
    {
    xb = B4x;
    yb = B4y;
    } 

}

if ( ( (sqrt(pow(xb-limax1x,2)+pow(yb-limax1y,2)))  < sqrt(pow(xb-limax2x,2)+pow(yb-limax2y,2))) ) 
{
    theta_b = atan2(yb-y_o,xb-x_o);
    if (theta_b <0) 
    {
        psip= 2*(M_PI - abs(theta_b));
        theta_b = abs(theta_b)+psip;
    }
    
    theta_lim =atan2(limax1y-y_o,limax1x-x_o);
    
    if (theta_lim <0)
     {
        psip= 2*(M_PI - abs(theta_lim));
        theta_lim = abs(theta_lim)+psip;
     }
    
    if (theta_lim > theta_b)
	{
        psi = (1/2)*(theta_lim-theta_b);
        xb = R1*cos(theta_b+psi)+x_o;
        yb = R1*sin(theta_b+psi)+y_o;
	}
    else
	{
        psi = (1/2)*(theta_b-theta_lim);
        xb = R1*cos(theta_b-psi)+x_o;
        yb = R1*sin(theta_b-psi)+y_o;
    	}
}
else
{
     theta_b = atan2(yb-y_o,xb-x_o);
    if (theta_b <0) 
    {
        psip= 2*(M_PI - abs(theta_b));
        theta_b = abs(theta_b)+psip;
    }
    
    theta_lim =atan2(limax2y-y_o,limax2x-x_o);
    
    if (theta_lim <0)
     {
        psip= 2*(M_PI - abs(theta_lim));
        theta_lim = abs(theta_lim)+psip;
     }
    
    if (theta_lim > theta_b)
	{
        psi = (1/2)*(theta_lim-theta_b);
        xb = R1*cos(theta_b+psi)+x_o;
        yb = R1*sin(theta_b+psi)+y_o;
	}
    else
	{
        psi = (1/2)*(theta_b-theta_lim);
        xb = R1*cos(theta_b-psi)+x_o;
        yb = R1*sin(theta_b-psi)+y_o;
    	}
}

  *(B+0)=xb;
  *(B+1)=yb;

  return B;

 }

////////////////////////////////////////////////////////////////////////////////////////////

double* CollAvoid(double x_p,double y_p,double theta_p, double k, double dmin, double dmax, double xb, double yb)
{
//Collision Avoidance gains and parameters
//ca stands for collision avoidance
    double * control;
    double k3,eta,kd,kca;
    double alpha, gamma, theta_i,y2,y__b,zita;
    int * segno;

    control = (double*)calloc(2,sizeof(double));

// Stabilization gains
   //printf("ColAvoidance \n");
   k3 = 1;
   eta = 1;
   kd = 0.05;
   kca = (k3*(sqrt(M_PI)*(dmax-dmin)*(1+kd*(1+dmax)))/((1-dmin)*(1+kd*(1-dmin)))+pow(2,-(3/4))*eta*((1+kd*(1+dmax))/(1-dmin)));


  alpha = theta_p - atan2(y_p,x_p) - M_PI;
  gamma = atan2((yb-y_p),(xb-x_p)) - theta_p;
  theta_i = atan2(y_p-obs_y,x_p-obs_x);


  if (alpha>M_PI)  {alpha=alpha-2*M_PI;}
  if (alpha<-M_PI) {alpha=alpha+2*M_PI;}

  if (gamma>M_PI)  {gamma=gamma-2*M_PI;}
  if (gamma<-M_PI) {gamma=gamma+2*M_PI;}

  zita = fmax(abs(gamma),pow(abs(gamma),0.5));

  /*
  if isnan(gamma_v)
  gamma_dot = 0;
  else
  gamma_dot = (gamma-gamma_v)/0.1;
  */
    
  y2 = 1/(sqrt(pow((x_p-obs_x),2)+pow((y_p-obs_y),2)));
 
  y__b = (sqrt(pow((x_p-xb),2)+pow((y_p-yb),2)));

	//printf("cos(alpha)=%g\n",cos(alpha));

//if  ((abs(gamma)<k*M_PI) && (cos(alpha) >= 0))
if  ((abs(gamma)<k*M_PI))
   { segno = sign(gamma);
    *(control+0) = k3*(1/y2);    
    //w = kp*gamma+kd*gamma_dot+ks*sqrt(abs(gamma))*sign(gamma)+(sin(gamma)/yb)*v;
    *(control+1) = kca*zita*(*(segno))+(sin(gamma)/y__b)*(*(control+0));
    }
else
   {segno = sign(gamma);
    *(control+0) = 0;
    //w = kp*gamma+kd*gamma_dot+ks*sqrt(abs(gamma))*sign(gamma)+(sin(gamma)/yb)*v;
    *(control+1) = kca*zita*(*(segno))+(sin(gamma)/y__b)*(*(control+0));
   }
   

    if (*(control+0)>0.5)  {*(control+0)=0.5;}
    if (*(control+0)<-0.5) {*(control+0)=-0.5;}

    //check max angular
    if (*(control+1)>5) {*(control+1)=5;}
    if (*(control+1)<-5) {*(control+1)=-5;}

    //check min angular
    if ( (*(control+1)>0) && (*(control+1)<0.5) ) {*(control+1)=0.5;}
    if ( (*(control+1)>-0.5) && (*(control+1)<0)) {*(control+1)=-0.5;}
     
    // printf("c0: %1e \n",*(control+0));
     //printf("c1: %1e \n",*(control+1));

    return control;

}

/////////////////////////////////////////////////////////////////////////////////////////
//Control to arrive to the destination
double* Stabilization(double x_p,double y_p,double theta_p, double k, double dmin, double dmax)
{   
    double * control;
    double k1,eta,k2;
    double alpha, zita;
    int * segno=NULL;

    control = (double*)calloc(2,sizeof(double));

// Stabilization gains
   //printf("Stabilization \n");
   k1=1;
   eta=1;
   k2=(((1+dmax)*k1+pow(2,(3/4))*eta)/(1-dmin));

// Control
    //printf("M_pi %1e \n",M_PI);
    alpha = theta_p - atan2(y_p,x_p) - M_PI;
    //printf("alpha pre %1e \n",alpha);
    
    if (alpha>M_PI)  {alpha=alpha-2*M_PI;}
    if (alpha<-M_PI) {alpha=alpha+2*M_PI;}

    zita = fmax(abs(alpha),pow(abs(alpha),0.5));

    if ((abs(alpha)) <= k*M_PI)
       { segno = sign(alpha);
	*(control+0) = k1*sqrt(pow(x_p,2)+pow(y_p,2))*cos(alpha);
        *(control+1) = -k2*zita*(*(segno)); }
    else
       {segno = sign(alpha);
	*(control+0) = 0;
        *(control+1) = -k2*zita*(*(segno));}
    
    if (*(control+0)>0.8)  {*(control+0)=0.8;}
    if (*(control+0)<-0.8) {*(control+0)=-0.8;}

    if (*(control+1)>5) {*(control+1)=5;}
    if (*(control+1)<-5) {*(control+1)=-5;}
     
     printf("c0: %1e \n",*(control+0));
     printf("c1: %1e \n",*(control+1));

    return control;
    
}

int main (int argc, char **argv)
{

  ros::init(argc,argv,"CA_hm_full_8");
  ros::NodeHandle n;
  ros::Rate loop_rate(10); //10Hz
  ros::Publisher  pub_cmd=n.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/teleop",100);
  ros::Publisher  pub_B=n.advertise<matteocontroler::Bpoint>("bpoint",100);
  //ros::Subscriber sub1 = n.subscribe("slam_out_pose",100, read_odometry);
  ros::Subscriber sub1 = n.subscribe("amcl_pose",100, read_odometry);
  ros::Subscriber sub2 = n.subscribe("scan",100, read_laser);
  geometry_msgs::Twist to_pub; 
  matteocontroler::Bpoint tobe_bpoint;
  ros::Time begin = ros::Time::now();
  double begin_sec = begin.toSec();
  
  double * stabil=NULL;
  double * CAv=NULL;
  double dmin = -0.5;
  double dmax = 0.5;
  double k = 0.03;
  double * bpoint=NULL;
  double norm = 0;
  double bx,by;
  double safety = 10,min = 10, obsG_x, obsG_y;
  int FLAG = 0, COUNT = 1, SAFE =1;
  int stat = 0;

printf("xPos = %g\tyPos = %g\n",xPos,yPos);
printf("FLAG = %d\n",FLAG);
		      
//main program
while (ros::ok())
  {

    if (sqrt(pow(xPos,2)+pow(yPos,2))>0.1)
		    { 
		      min = sqrt(pow(obs_x,2)+pow(obs_y,2));
		      //printf("min = %g\n",min);
		      //printf("FLAG = %d\n",FLAG);

		      if ((min<=0.60) && (FLAG != 1))
		       {
			//printf("min inside if = %g\n",min);
		        //printf("FLAG inside if first= %d\n",FLAG);
			FLAG = 1;
			COUNT = 0;
                        SAFE = 1;
                        //printf("FLAG inside if second = %d\n",FLAG);
			}
		      
		      
		       if (COUNT == 0){  //printf("Calcolo B\n");
					obsG_x = xPos+ min*cos(wPos+obs_lw);
					obsG_y = yPos+ min*sin(wPos+obs_lw);

					bpoint = Bpoint(xPos,yPos,wPos,obsG_x,obsG_y,dmax);
					bx=*(bpoint+0);
					by=*(bpoint+1);
				        COUNT = 1;
					/*
					printf("min = %g;\n",min);
					printf("xPos =%g;\n",xPos);
					printf("yPos =%g;\n",yPos);
					printf("wPos =%g;\n",wPos);
					printf("obsG_x =%g;\n",obsG_x);
					printf("obsG_y =%g;\n",obsG_y);
					printf("obs_r =%g;\n",obs_r);
					printf("B= [%g,%g];\n",bx, by);
					*/
		
					norm = sqrt(pow(xPos-bx,2)+pow(yPos-by,2));
					//printf("norm:%g\n",norm);
					}
		   
		       
			if ((FLAG == 1)&&(norm > 0.10))
					{
					 
					 safety = sqrt(pow(obs_x,2)+pow(obs_y,2));
					 if (safety < 0.30)
					 {
					 printf("SAFETY STOP %g\n",safety);
					 to_pub.linear.x = 0;
					 to_pub.angular.z = 0;
					 pub_cmd.publish(to_pub);
                                         FLAG = 0;
                                         COUNT = 0;
                                         SAFE = 0;
                                         stat = 2;
					 }
					 else
					{
					 //printf("CA\n");
					 //printf("pos: %g,%g,%g;\n",xPos,yPos,wPos);
					 
					 CAv = CollAvoid(xPos,yPos,wPos,k,dmin,dmax, bx,by);
					 to_pub.linear.x = *(CAv+0);
					 to_pub.angular.z = *(CAv+1);
					 pub_cmd.publish(to_pub);

					 norm = sqrt(pow(xPos-bx,2)+pow(yPos-by,2));
					 //printf("norm =%g;\n",norm);
					 stat = 1;
					 }

					}
			else 
			    {
			      FLAG = 0;
			      //COUNT = 1; 
			     }
		
		      
		      
		    if ((FLAG == 0) && (SAFE == 1))
			    {
                             //printf("inside stab\n");
			    stabil = Stabilization(xPos,yPos,wPos,k,dmin,dmax);
			    to_pub.linear.x = *(stabil+0);
			    to_pub.angular.z = *(stabil+1);
			    pub_cmd.publish(to_pub);
			    stat = 0;
			    } 
		   }
        

	begin = ros::Time::now();
        begin_sec = begin.toSec();
        
	tobe_bpoint.time=begin_sec ;
	tobe_bpoint.xPos=xPos;
	tobe_bpoint.yPos=yPos;
	tobe_bpoint.wPos=wPos;
	tobe_bpoint.obs_x=obs_x;
	tobe_bpoint.obs_y=obs_y;
	tobe_bpoint.obs_r=obs_r;
	tobe_bpoint.Bx=bx;
	tobe_bpoint.By=by;
	tobe_bpoint.stat=stat;
	tobe_bpoint.norm=norm;
	pub_B.publish(tobe_bpoint);


   if (sqrt(pow(xPos,2)+pow(yPos,2))<=0.1)
    {printf("Task Completed\n");
    to_pub.linear.x = 0;
    to_pub.angular.z = 0;
    pub_cmd.publish(to_pub);
    }

    ros::spinOnce();
    loop_rate.sleep();
    }
  return 0;
}
  
