/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */

#include "unicycle_control/Teleop.hpp"
#include "unicycle_control/mauve/Teleop.hpp"

namespace unicycle_control {

using namespace ::mauve::types::sensor;
using namespace ::mauve::types::geometry;

bool buttonPressed(const Joy& j, int b) {
  if (j.buttons.size() > b)
    return (j.buttons[b] != 0);
  else
    return false;
}

double joystickAxis(const Joy& j, int a) {
  if (j.axes.size() > a)
    return j.axes[a];
  else
    return 0;
}

namespace mauve {

  void TeleopCore::update() {
    UnicycleVelocity v {0, 0};
    auto j = shell().joystick.read();

    if (j.status == ::mauve::runtime::DataStatus::NEW_DATA) {

      if (buttonPressed(j.value, shell().stop_button) != shell().inverse_stop) {
        logger().info("stopping robot");
        shell().stop();
        shell().command.write(v);
        return;
      }

      v.linear = joystickAxis(j.value, shell().linear_axis);
      v.angular = joystickAxis(j.value, shell().angular_axis);

      if (buttonPressed(j.value, shell().fast_button)) {
        v.linear *= 2;
        //v.angular *= 2;
      }

      shell().command.write(v);
    }
  }

}

}
