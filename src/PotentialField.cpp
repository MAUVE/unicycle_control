/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#include <limits>
#include "unicycle_control/PotentialField.hpp"
#include "unicycle_control/mauve/PotentialField.hpp"
#include "unicycle_control/common.hpp"

namespace unicycle_control {

  using namespace ::mauve::types::sensor;
  using namespace ::mauve::types::geometry;

  ::mauve::types::geometry::Point2D PotentialField::obstacleFromScan(const LaserScan& scan, double far) {
    double xmin = std::numeric_limits<double>::max();
    double xmax = std::numeric_limits<double>::min();
    double ymin = std::numeric_limits<double>::max();
    double ymax = std::numeric_limits<double>::min();

    for (int i = 0; i < scan.ranges.size(); i++) {
      if (scan.ranges[i] < scan.range_min) continue;

      if (scan.ranges[i] <= far) {
         double dist = scan.ranges[i];
         double ang = scan.angle_min + (scan.angle_max - scan.angle_min) * i / scan.ranges.size();
         double px = dist * cos(ang);
         double py = dist * sin(ang);
         if (px < xmin) xmin = px;
         if (px > xmax) xmax = px;
         if (py < ymin) ymin = py;
         if (py > ymax) ymax = py;
       }
     }

     ::mauve::types::geometry::Point2D obstacle;
     obstacle.x = xmin;
     obstacle.y = ymin;
     obstacle_lw = atan2(obstacle.y, obstacle.x);
     obstacle_radius = far;
     return obstacle;
  }

  UnicycleVelocity PotentialField::PF(double x_p, double y_p,
    double theta_p, double obs_dx, double obs_dy,
    double dist_safe, double nu, double psi, double epsilon, double alpha,
    double* dist_g)
  {
    UnicycleVelocity control;

    if (theta_p < -M_PI)
      theta_p = theta_p + 2*M_PI;
    else if (theta_p >M_PI)
      theta_p = theta_p - 2*M_PI;

    *dist_g = sqrt(pow(x_p,2)+pow(y_p,2));
    logger().debug("dist_g = {}", *dist_g);

    double dist_o = sqrt(pow((x_p-obs_dx),2)+pow((y_p-obs_dy),2));
    logger().debug("dist_o = {}", dist_o);

    ///////// attractive force
    double Fatt1=0, Fatt2=0;
    if (*dist_g <= nu) {
      Fatt1 = -2*x_p;
      Fatt2 = -2*y_p;
    }
    if (*dist_g > psi) {
      Fatt1 = -x_p/ *dist_g;
      Fatt2 = -y_p/ *dist_g;
    }
    logger().debug("Fatt1={}, Fatt2={}", Fatt1, Fatt2);

    ///////// repulsive force
    double Frep1=0, Frep2=0;
    if (dist_o < dist_safe) {
      double value = pow(dist_safe,2) - pow(dist_o,2);
    	Frep1 = 4 * alpha * fmax(0,value) * (x_p - obs_dx);
      Frep2 = 4 * alpha * fmax(0,value) * (y_p - obs_dy);
    }
    logger().debug("Frep1={}, Frep2={}", Frep1, Frep2);

    ///////// composition
    double F1 = Fatt1 +Frep1;
    double F2 = Fatt2 +Frep2;

    //////// Eventual V
    double V1=0, V2=0;
    if (sqrt(pow(F1,2) + pow(F2,2)) <= epsilon) {
    	double rho = y_p - (obs_dy/obs_dx) * x_p;
      int s = sign(rho);
    	V1 = ( (s*epsilon) / *dist_g ) * y_p;
    	V2 = ( (s*epsilon) / *dist_g ) * (-x_p);
    }
    logger().debug("V1={}, V2={}", V1, V2);

    /////////
    F1 = Fatt1 + Frep1 - V1;
    F2 = Fatt2 + Frep2 - V2;
    logger().debug("F1={}, F2={}", F1, F2);

    double theta_d = atan2(F2, F1);
    logger().debug("theta_d={}", theta_d);
    double err = theta_d - theta_p;
    if (err < -M_PI)
      err = err + 2*M_PI;
    else if (err > M_PI)
      err = err - 2*M_PI;
    logger().debug("err={}", err);
    int s = sign(err);

    control.angular =  sqrt(fabs(err)) * s; // * wmax = 1
    control.linear = (1 /* umax */ / (1+epsilon)) * (sqrt(pow(F1,2)+pow(F2,2)));
    logger().debug("control={},{}", control.linear, control.angular);

    return control;
  }

namespace mauve {
  using namespace ::mauve::runtime;

  void PotentialFieldCore::read_input_goal() {
    auto g = shell().goal.read();
    if (g.status == DataStatus::NEW_DATA) {
      goal = g.value;
      logger().info("Received new goal {}", goal);
      _has_goal = true;
    }
  }

  void PotentialFieldCore::stop() {
    shell().command.write({0, 0});
    shell().arrived.write(goal);
    _has_goal = false;
  }

  void PotentialFieldCore::update() {
    robot_pose = shell().pose.read();
    obstacle = obstacleFromScan(shell().scan.read(), shell().obstacle_distance.get());

    double xPos = robot_pose.location.x - goal.x;
    double yPos = robot_pose.location.y - goal.y;
    double wPos = robot_pose.theta;
    if ( wPos < 0) wPos += 2 * M_PI;

    double obs_dist = sqrt(pow(obstacle.x,2) + pow(obstacle.y,2));
    logger().debug("obstacle {} distance: {}", obstacle, obs_dist);
    this->shell().obstacle.write(Pose2D { obstacle, obstacle_radius });
    this->shell().obstacle_distance_port.write(obs_dist);

    _arrived = false;
    UnicycleVelocity cmd {0, 0};

    if (sqrt( pow(xPos,2) + pow(yPos,2) ) < shell().goal_tolerance) {
      logger().info("Arrived at goal {}", goal);
      _arrived = true;
    }
    else {
      double obsG_x = (xPos) + obs_dist*cos(wPos + obstacle_lw);
      double obsG_y = (yPos) + obs_dist*sin(wPos + obstacle_lw);
      double dist_g;
      cmd = PF(xPos, yPos, wPos, obsG_x, obsG_y,
        shell().safety_distance.get(), this->nu.get(), this->psi.get(),
        this->epsilon.get(), this->alpha.get(), &dist_g );
      shell().distance_to_goal.write(dist_g);
      logger().debug("command: {}", cmd);
    }

    shell().command.write(cmd);
  }

  void PotentialFieldCore::cleanup_hook() {
      shell().command.write({0, 0});
  };

}
}
