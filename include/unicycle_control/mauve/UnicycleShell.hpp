/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_UNICYCLE_CONTROL_SHELL_HPP
#define MAUVE_UNICYCLE_CONTROL_SHELL_HPP

#include <mauve/runtime.hpp>
#include <mauve/types.hpp>

namespace unicycle_control {
namespace mauve {

    /**
     * Basic Shell for Unicycle Robot Control
     */
     struct UnicycleControlShell : public ::mauve::runtime::Shell {
      /** Distance at which a point is considered reached */
      ::mauve::runtime::Property<double> & goal_tolerance =
        mk_property("goal_tolerance", 0.2);
      /** Input port with the robot pose */
      ::mauve::runtime::ReadPort< ::mauve::types::geometry::Pose2D > & pose =
        mk_read_port<::mauve::types::geometry::Pose2D>("pose", ::mauve::types::geometry::Pose2D());
      /** Input port with the control target */
      ::mauve::runtime::ReadPort< ::mauve::runtime::StatusValue<::mauve::types::geometry::Point2D> > & goal =
        mk_read_port<::mauve::runtime::StatusValue<::mauve::types::geometry::Point2D>>("goal",
          ::mauve::runtime::StatusValue<::mauve::types::geometry::Point2D> {
            ::mauve::runtime::DataStatus::NO_DATA, ::mauve::types::geometry::Point2D() });

      /** Output port with the computed command */
      ::mauve::runtime::WritePort< ::mauve::types::geometry::UnicycleVelocity > & command =
        mk_write_port<::mauve::types::geometry::UnicycleVelocity>("command");
      /** Output port with the remaining distance to the target. Can be used for motion monitoring. */
      ::mauve::runtime::WritePort< double > & distance_to_goal =
        mk_write_port<double>("distance_to_goal");
      /** Output port when arrived at goal */
      ::mauve::runtime::WritePort<::mauve::types::geometry::Point2D> & arrived =
        mk_write_port<::mauve::types::geometry::Point2D>("arrived");
    };

    /**
     * Shell for Unicycle Control and Obstacle Avoidance
     */
    struct ObstacleAvoidanceShell : public UnicycleControlShell {
      /** Distance at which obstacles are considered */
      ::mauve::runtime::Property<double> & obstacle_distance =
        mk_property("obstacle_distance", 0.8);
      /** Safety distance with respect to obstacles */
      ::mauve::runtime::Property<double> & safety_distance =
        mk_property("safety_distance", 0.8);
      /** Input port with a Laser scan to avoid obstacles */
      ::mauve::runtime::ReadPort< ::mauve::types::sensor::LaserScan > & scan =
        mk_read_port<::mauve::types::sensor::LaserScan>("scan",
          ::mauve::types::sensor::LaserScan() );
      /** Output port for obstacle data for debug */
      ::mauve::runtime::WritePort< ::mauve::types::geometry::Pose2D > & obstacle =
        mk_write_port<::mauve::types::geometry::Pose2D>("obstacle");
      /** Output port for obstacle data for debug */
      ::mauve::runtime::WritePort<double> & obstacle_distance_port =
        mk_write_port<double>("obstacle_distance");
    };

  }
}

#endif // MAUVE_UNICYCLE_CONTROL_SHELL_HPP
