/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef UNICYCLE_CONTROL_PF_HPP
#define UNICYCLE_CONTROL_PF_HPP

#include <mauve/types/geometry_types.hpp>
#include <mauve/types/sensor_types.hpp>
#include <mauve/runtime.hpp>

namespace unicycle_control {

class PotentialField : virtual public ::mauve::runtime::WithLogger {
protected:
  ::mauve::types::geometry::UnicycleVelocity PF(double x_p, double y_p,
    double theta_p, double obs_dx, double obs_dy,
    double dist_safe, double nu, double psi, double epsilon, double alpha,
    double* dist_to_goal);

  ::mauve::types::geometry::Point2D obstacleFromScan(const ::mauve::types::sensor::LaserScan&, double safety_distance);

  double obstacle_lw, obstacle_radius;

};

}


#endif // MAUVE_UNICYCLE_CONTROL_PF_HPP
