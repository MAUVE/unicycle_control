/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_UNICYCLE_CONTROL_REBOUND_HPP
#define MAUVE_UNICYCLE_CONTROL_REBOUND_HPP

#include <mauve/base/PeriodicStateMachine.hpp>

#include "UnicycleShell.hpp"

namespace mauve {
  namespace unicycle_control {

    struct ReboundCore : public runtime::Core<ObstacleAvoidanceShell> {
    public:
      void update();

    private:
      types::geometry::Pose2D robot_pose;
      types::geometry::Point2D goal;
      types::geometry::Point2D rebound_point;

      double alpha;
      double d;

      types::geometry::UnicycleVelocity command(const types::geometry::Pose2D&,
        const types::geometry::Point2D&);
      types::geometry::Point2D reboundPoint();
      bool obstacleFromScan(const types::sensor::LaserScan&);

    };

    using Rebound = runtime::Component< ObstacleAvoidanceShell,
      ReboundCore, base::PeriodicStateMachine<ObstacleAvoidanceShell,
        ReboundCore> >;

  }
}


#endif // MAUVE_UNICYCLE_CONTROL_PF_HPP
